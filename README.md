# Реактивная контрольная панель для Elite Dangerous (v1.1.2)

o7, CMDR. [Let me speak from my heart in English.](https://translate.google.com/translate?sl=ru&tl=en&u=https://gitlab.com/Vadimhtml/edeck/-/blob/main/README.md)

![v1.1 Sequence](docs/sequence_l1.jpg)

Видео: [v1.1 Sequence](docs/demo_sequence.mp4) | [v1.0 Default](docs/demo.mp4)

## Возможности

1. Использование любого устройства с веб-браузером в качестве реагирующей на игровую обстановку контрольной панели для
   ПК версии **Elite Dangerous: Odyssey**;
2. Создание собственных реализаций с использованием исключительно HTML и CSS;

## Установка

1. Вам нужен [Python](https://www.python.org/downloads/) версии 3;
2. Скачать и распаковать или клонировать этот проект на ПК, где установлена Elite Dangerous: Odyssey;

## Запуск

1. В директории проекта выполнить команду `py ./edeck.py`;
2. Сервер сконфигурируется автоматически и покажет адрес, по которому он запущен;
    - Ручное конфигурирование можно выполнить в файле `config.ini`, в корне проекта;
3. С любого устройства, подключенного к той же сети, перейти по адресу запущеного сервера;

*Для Android устройств удобно
использовать [Fully Kiosk Browser](https://play.google.com/store/apps/details?id=de.ozerov.fully), который позволяет
запускать веб-страницы в полноэкранном режиме.*

## Скриншоты

### v1.1 Sequence
- [Интерфейс](docs/sequence_l1.jpg)

### 1.0 Default

#### Портретная ориентация

- [Интерфейс](docs/p1.png)
- [Грузовой кофш выпущен и автопилот отключен](docs/p2.png)
- [Тихий ход и перегрев](docs/p3.png)
- [Гравитационный захват](docs/p4.png)
- [РСД остывает](docs/p5.png)

#### Альбомная ориентация

- [Интерфейс](docs/l1.png)
- [Грузовой кофш выпущен и автопилот отключен](docs/l2.png)
- [Тихий ход и перегрев](docs/l3.png)
- [Гравитационный захват](docs/l4.png)
- [РСД остывает](docs/l5.png)

## Разработка собственной контрольной панели

- [Документация по разработке](docs/DEVELOPMENT.md)