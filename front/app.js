function app() {

    const action_fn = (key) => {
        const key_xhr = new XMLHttpRequest();
        key_xhr.open('GET', '/key/' + key);
        key_xhr.send();
        key_xhr.onload = null;
        key_xhr.onerror = null;
    }

    Array.from(document.querySelectorAll('[data-key]')).forEach((el) => {
        el.addEventListener('click', () => {
            action_fn(el.dataset.key);
        })
    });


    // Bit functions
    const bit_test = (num, bit) => (num >> bit) & 0x1;
    const bit_set = (num, bit) => num | 1 << bit;
    const bit_clear = (num, bit) => num & ~(1 << bit);
    const bit = (num, bit, val) => val ? bit_set(num, bit) : bit_clear(num, bit);

    // Switch function for template
    const s = (...args) => {
        for (let i = 0; i < args.length; i++) {
            if (args[i]) return i + 1;
        }
        return 0;
    };

    let o_flags;
    const received = (n_flags) => {
        if (n_flags !== o_flags) {
            o_flags = n_flags;
            const flags = o_flags.split(',').map(Number);

            // 0.0 - disconnected, 0.1 - fail, 0.2 - success, 0.3 - game not started
            if (flags[1] === 0 && flags[2] === 0) {
                flags[0] = bit_set(flags[0], 3);
            }

            Array.from(document.querySelectorAll('[data-react]')).forEach((el) => {
                const react = el.dataset.react;
                const state = eval(react.replaceAll(/\d.\d+/g, (ndl) => {
                    const [num, bit] = ndl.split('.').map(Number);
                    return (bit_test(flags[num], bit));
                }));
                el.dataset.state = String(Number(state));
            });
        }
        return true;
    }


    // POOL BEGIN //////////////////////////////////////////////////////////////////////////////////////////////////////
    const pool_xhr = new XMLHttpRequest();
    const pool_timeout = 100;
    let pool_fails = 0;

    const pool = () => {
        // 0.0 - disconnected, 0.1 - fail, 0.2 - success (1, 2, 4)
        pool_xhr.open('GET', '/flags');
        pool_xhr.send();
        pool_xhr.onload = () => {
            if (pool_xhr.status === 200) {
                received("4," + pool_xhr.responseText);
            } else {
                pool_fails++;
                if (pool_fails > 10) {
                    pool_fails = 0;
                    received("2,0,0");
                }
            }
            setTimeout(pool, pool_timeout);
        };
        pool_xhr.onerror = function () {
            received("1,0,0");
            setTimeout(pool, pool_timeout);
        };
    }
    pool();
    // POOL END ////////////////////////////////////////////////////////////////////////////////////////////////////////

}

window.onload = app;