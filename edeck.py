import configparser
import os
import threading
import json
import time
import socket
import sys
import re
import ctypes
from urllib.parse import unquote
from http.server import HTTPServer, BaseHTTPRequestHandler

os.system('cls')

config = configparser.ConfigParser()
config.read("config.ini")

cf_ip = config.get("EDeck", "ip")
cf_port = config.get("EDeck", "port")
cf_path = config.get("EDeck", "path")

if cf_ip == "auto":
    cf_ip = socket.gethostbyname(socket.gethostname())

if cf_port == "default":
    cf_port = 8000

if cf_path == "auto":
    cf_path = os.path.expanduser("~") + r"\Saved Games\Frontier Developments\Elite Dangerous\Status.json"

user32 = ctypes.WinDLL('user32', use_last_error=True)

R_OK = 200
R_NOT_FOUND = 404

CT_HTML = "text/html"
CT_SVG = "image/svg+xml"

model = {"mtime": float(0), "hash": 0, "flags": "0,0", "error": False}

kdict = {
    "esc": 1,
    "1": 2,
    "2": 3,
    "3": 4,
    "4": 5,
    "5": 6,
    "6": 7,
    "7": 8,
    "8": 9,
    "9": 10,
    "0": 11,
    "-": 12,
    "=": 13,
    "backspace": 14,
    "tab": 15,
    "q": 16,
    "w": 17,
    "e": 18,
    "r": 19,
    "t": 20,
    "y": 21,
    "u": 22,
    "i": 23,
    "o": 24,
    "p": 25,
    "[": 26,
    "]": 27,
    "enter": 28,
    "left ctrl": 29,
    "a": 30,
    "s": 31,
    "d": 32,
    "f": 33,
    "g": 34,
    "h": 35,
    "j": 36,
    "k": 37,
    "l": 38,
    ";": 39,
    "'": 40,
    "`": 41,
    "left shift": 42,
    "\\": 43,
    "z": 44,
    "x": 45,
    "c": 46,
    "v": 47,
    "b": 48,
    "n": 49,
    "m": 50,
    "comma": 51,
    ".": 52,
    "/": 53,
    "right shift": 54,
    "num *": 55,
    "left alt": 56,
    "space": 57,
    "caps lock": 58,
    "f1": 59,
    "f2": 60,
    "f3": 61,
    "f4": 62,
    "f5": 63,
    "f6": 64,
    "f7": 65,
    "f8": 66,
    "f9": 67,
    "f10": 68,
    "num lock": 69,
    "scroll lock": 70,
    "num 7": 71,
    "num 8": 72,
    "num 9": 73,
    "num -": 74,
    "num 4": 75,
    "num 5": 76,
    "num 6": 77,
    "num add": 78,
    "num 1": 79,
    "num 2": 80,
    "num 3": 81,
    "num 0": 82,
    "num .": 83,
    "f11": 87,
    "f12": 88,
    "num /": 181,
    "right alt": 184,
    "pause": 197,
    "home": 199,
    "up arrow": 200,
    "page up": 201,
    "left arrow": 203,
    "right arrow": 205,
    "end": 207,
    "down arrow": 208,
    "page down": 209,
    "insert": 210,
    "delete": 211,
}


# ############################################################################################## KEYBOARD ##############
def keyboard(sequence):
    def keyboard_action(action, direction):
        time.sleep(0.05)
        mod = 0
        if action > 128:
            action = action - 128
            mod = 1
        user32.keybd_event(0, action, direction + mod, 0)
        return

    sequence = list(map(str.strip, sequence.split(',')))
    for combination in sequence:
        combination = list(map(str.strip, combination.split('+')))
        codes = []
        for shortcut in combination:
            code = kdict.get(shortcut)
            if code:
                codes.append(code)
            else:
                print("Unknown key: " + shortcut)

        for code in codes:
            keyboard_action(code, 0)
        codes.reverse()
        for code in codes:
            keyboard_action(code, 2)
    return


# ############################################################################################## FORMATTED TIME ########
def ftime():
    return time.strftime("%H:%M:%S")


# ############################################################################################# READ STATUS ############
def read_status():
    while True:
        try:
            mtime = os.path.getmtime(cf_path)
            if model["mtime"] != mtime or model["error"]:
                model["mtime"] = mtime
                with open(cf_path, "r") as file:
                    data = file.read()
                    file_hash = hash(data)
                    if model["hash"] != file_hash or model["error"]:
                        model["hash"] = file_hash
                        data = json.loads(data)

                        if "Flags" in data:
                            if "Flags2" not in data:
                                data["Flags2"] = 0
                            model["flags"] = str(data["Flags"]) + "," + str(data["Flags2"])
                        else:
                            model["flags"] = "0,0"

                        model["error"] = False
                        print(ftime() + " - MODEL: " + str(model))
        except (IOError, ValueError):
            if not model["error"]:
                print(ftime() + " - ERROR: IOError, File open, JSON")
                model["error"] = True
        time.sleep(0.05)


# ############################################################################################# S ######################
class S(BaseHTTPRequestHandler):

    def _write(self, state, content_type, data):
        try:
            self.send_response(state)
            self.send_header("Content-type", content_type)
            if content_type == CT_SVG:
                self.send_header("Cache-Control", "max-age=31536000")
            self.end_headers()
            self.wfile.write(data.encode("utf8"))
        except IOError:
            print(ftime() + " - ERROR: IOError, HTTP Send")
        return

    def log_message(self, log_format, *args):
        return

    def do_GET(self):  # noqa
        url = self.path

        if url == "/flags":
            if model["error"]:
                self._write(R_NOT_FOUND, CT_HTML, model["flags"])
            else:
                self._write(R_OK, CT_HTML, model["flags"])
            return

        req = re.match("^/$|^/theme/([a-z]+)/$", url)
        if req:
            if req.group(1):
                theme = req.group(1)
            else:
                theme = "default"

            with open(f"themes/{theme}/index.html", "r") as file:
                data = file.read()

            head = data.find("</head>")
            if head >= 0:
                with open("front/app.js", "r") as js_file:
                    js_data = js_file.read()
                    data = data[:head] + f"\t<script>\n{js_data}\n\t</script>\n" + data[head:]
                with open(f"themes/{theme}/index.css", "r") as css_file:
                    css_data = css_file.read()
                    data = data[:head] + f"\t<style>\n{css_data}\n\t</style>\n" + data[head:]
                data = data[:head] + f"\t<base href='http://{cf_ip}:{cf_port}/theme/{theme}/'/>\n" + data[head:]  # noqa

            self._write(R_OK, CT_HTML, data)
            return

        req = re.match("^/theme/([a-z]+)/img/([a-z-]+.svg)$", url)
        if req:
            with open(f"themes/{req.group(1)}/img/{req.group(2)}", "r") as file:
                data = file.read()
            self._write(R_OK, CT_SVG, data)
            return

        press = re.match('^/key/(.+)$', url)
        if press:
            key_string = unquote(press.group(1))
            keyboard(key_string)
            self._write(R_OK, CT_HTML, "ok")
            return

        self._write(R_NOT_FOUND, CT_HTML, "404 Not found")


# ############################################################################################# SERVE ##################
def serve():
    httpd = HTTPServer((cf_ip, cf_port), S)
    httpd.serve_forever()


# ############################################################################################# MAIN ###################
if __name__ == "__main__":
    thread_read_status = threading.Thread(target=read_status)
    thread_serve = threading.Thread(target=serve)
    thread_read_status.setDaemon(True)
    thread_serve.setDaemon(True)

    print("o7, CMDR")
    print(f"Watch: {cf_path}")
    print("\nThemes:")

    themes = [f for f in os.listdir("themes") if os.path.isdir(os.path.join("themes", f))]
    addr = f"http://{cf_ip}:{cf_port}/"  # noqa

    print(addr)
    for theme in themes:
        if theme != "default":
            print(f"{addr}theme/{theme}/")

    print("\nLog:")

    thread_read_status.start()
    thread_serve.start()

    while True:
        try:
            time.sleep(0.2)
        except (KeyboardInterrupt, SystemExit):
            print("Received keyboard interrupt, quitting threads.")
            sys.exit()
